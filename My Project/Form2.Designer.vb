﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Me.Sanitat = New System.Windows.Forms.PictureBox()
        Me.ONU1 = New System.Windows.Forms.PictureBox()
        Me.Persona = New System.Windows.Forms.TextBox()
        Me.Hospitals = New System.Windows.Forms.TextBox()
        Me.Aliments = New System.Windows.Forms.TextBox()
        Me.Clicar = New System.Windows.Forms.Button()
        Me.Clicar1 = New System.Windows.Forms.Button()
        Me.Clicar2 = New System.Windows.Forms.Button()
        Me.Ciudad = New System.Windows.Forms.PictureBox()
        Me.retornar = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        CType(Me.Sanitat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ONU1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Sanitat
        '
        Me.Sanitat.Image = CType(resources.GetObject("Sanitat.Image"), System.Drawing.Image)
        Me.Sanitat.Location = New System.Drawing.Point(-1, 0)
        Me.Sanitat.Name = "Sanitat"
        Me.Sanitat.Size = New System.Drawing.Size(82, 61)
        Me.Sanitat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Sanitat.TabIndex = 1
        Me.Sanitat.TabStop = False
        '
        'ONU1
        '
        Me.ONU1.Image = CType(resources.GetObject("ONU1.Image"), System.Drawing.Image)
        Me.ONU1.Location = New System.Drawing.Point(886, 0)
        Me.ONU1.Name = "ONU1"
        Me.ONU1.Size = New System.Drawing.Size(91, 61)
        Me.ONU1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ONU1.TabIndex = 3
        Me.ONU1.TabStop = False
        '
        'Persona
        '
        Me.Persona.Font = New System.Drawing.Font("Times New Roman", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Persona.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Persona.Location = New System.Drawing.Point(77, 67)
        Me.Persona.Multiline = True
        Me.Persona.Name = "Persona"
        Me.Persona.Size = New System.Drawing.Size(98, 28)
        Me.Persona.TabIndex = 6
        Me.Persona.Text = "Persones"
        '
        'Hospitals
        '
        Me.Hospitals.Font = New System.Drawing.Font("Times New Roman", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hospitals.ForeColor = System.Drawing.Color.Red
        Me.Hospitals.Location = New System.Drawing.Point(437, 67)
        Me.Hospitals.Multiline = True
        Me.Hospitals.Name = "Hospitals"
        Me.Hospitals.Size = New System.Drawing.Size(94, 28)
        Me.Hospitals.TabIndex = 7
        Me.Hospitals.Text = "Sanitat"
        '
        'Aliments
        '
        Me.Aliments.Font = New System.Drawing.Font("Times New Roman", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aliments.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Aliments.Location = New System.Drawing.Point(786, 67)
        Me.Aliments.Multiline = True
        Me.Aliments.Name = "Aliments"
        Me.Aliments.Size = New System.Drawing.Size(126, 28)
        Me.Aliments.TabIndex = 8
        Me.Aliments.Text = "Alimentació"
        '
        'Clicar
        '
        Me.Clicar.ForeColor = System.Drawing.Color.Black
        Me.Clicar.Location = New System.Drawing.Point(77, 111)
        Me.Clicar.Name = "Clicar"
        Me.Clicar.Size = New System.Drawing.Size(94, 38)
        Me.Clicar.TabIndex = 9
        Me.Clicar.Text = "TaulaPersona"
        Me.Clicar.UseVisualStyleBackColor = True
        '
        'Clicar1
        '
        Me.Clicar1.Location = New System.Drawing.Point(437, 111)
        Me.Clicar1.Name = "Clicar1"
        Me.Clicar1.Size = New System.Drawing.Size(94, 38)
        Me.Clicar1.TabIndex = 10
        Me.Clicar1.Text = "TaulaSanitat"
        Me.Clicar1.UseVisualStyleBackColor = True
        '
        'Clicar2
        '
        Me.Clicar2.Location = New System.Drawing.Point(786, 111)
        Me.Clicar2.Name = "Clicar2"
        Me.Clicar2.Size = New System.Drawing.Size(126, 38)
        Me.Clicar2.TabIndex = 11
        Me.Clicar2.Text = "TaulaAlimentació"
        Me.Clicar2.UseVisualStyleBackColor = True
        '
        'Ciudad
        '
        Me.Ciudad.Image = CType(resources.GetObject("Ciudad.Image"), System.Drawing.Image)
        Me.Ciudad.Location = New System.Drawing.Point(-1, 0)
        Me.Ciudad.Name = "Ciudad"
        Me.Ciudad.Size = New System.Drawing.Size(991, 473)
        Me.Ciudad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Ciudad.TabIndex = 0
        Me.Ciudad.TabStop = False
        '
        'retornar
        '
        Me.retornar.Location = New System.Drawing.Point(816, 429)
        Me.retornar.Name = "retornar"
        Me.retornar.Size = New System.Drawing.Size(143, 44)
        Me.retornar.TabIndex = 13
        Me.retornar.Text = "Return"
        Me.retornar.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.Bisque
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.GridColor = System.Drawing.SystemColors.Control
        Me.DataGridView1.Location = New System.Drawing.Point(12, 165)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(244, 244)
        Me.DataGridView1.TabIndex = 14
        '
        'DataGridView2
        '
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.PeachPuff
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(372, 165)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(244, 244)
        Me.DataGridView2.TabIndex = 15
        '
        'DataGridView3
        '
        Me.DataGridView3.BackgroundColor = System.Drawing.Color.PeachPuff
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(706, 165)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(244, 244)
        Me.DataGridView3.TabIndex = 16
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(962, 474)
        Me.Controls.Add(Me.DataGridView3)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.retornar)
        Me.Controls.Add(Me.Clicar2)
        Me.Controls.Add(Me.Clicar1)
        Me.Controls.Add(Me.Clicar)
        Me.Controls.Add(Me.Aliments)
        Me.Controls.Add(Me.Hospitals)
        Me.Controls.Add(Me.Persona)
        Me.Controls.Add(Me.ONU1)
        Me.Controls.Add(Me.Sanitat)
        Me.Controls.Add(Me.Ciudad)
        Me.Name = "Form2"
        Me.Text = "Form2"
        CType(Me.Sanitat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ONU1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ciudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Sanitat As PictureBox
    Friend WithEvents ONU1 As PictureBox
    Friend WithEvents Persona As TextBox
    Friend WithEvents Hospitals As TextBox
    Friend WithEvents Aliments As TextBox
    Friend WithEvents Clicar As Button
    Friend WithEvents Clicar1 As Button
    Friend WithEvents Clicar2 As Button
    Friend WithEvents Ciudad As PictureBox
    Friend WithEvents retornar As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents DataGridView3 As DataGridView
End Class
