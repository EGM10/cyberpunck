﻿Imports MySql.Data.MySqlClient
Public Class Form2
    Private Sub retornar_Click(sender As Object, e As EventArgs) Handles retornar.Click
        Form1.Show()
        Me.Hide()
    End Sub

    Dim connexio As MySqlConnection
    Dim query As String
    Public Property DataGrid1 As Object

    Private Sub Clicar_Click(sender As Object, e As EventArgs) Handles Clicar.Click
        Dim Persona As Persona
        connectar()
        afegirPersona()
        mostrarPersona()
        eliminarPersona()
        desconnectar()
    End Sub
    Private Sub afegirPersona()
        Dim nom As String
        nom = Clicar.Text
        query = "INSERT INTO `persona` (`id`, `Nom`, `1rCognom`, `2nCognom`, `Població`, `Data Naixament`) VALUES ('12', 'Joel', 'Viladomat', 'Mitjana', 'Lleida', '19960815')"
        Dim comanda = New MySqlCommand(query, connexio)
        comanda.ExecuteNonQuery()
    End Sub
    Private Sub eliminarPersona()
        Dim eliminar As String
        eliminar = Clicar.Text
        eliminar = "DELETE FROM persona WHERE nom='Fernando'"
        Dim comanda = New MySqlCommand(eliminar, connexio)
        comanda.ExecuteNonQuery()
    End Sub
    Private Sub connectar()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=persona"
            connexio.Open()
            MessageBox.Show("Connectat")
        Catch
            MessageBox.Show("Error")
        End Try
    End Sub
    Private Sub mostrarPersona()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=persona"
            connexio.Open()
            query = "SELECT * FROM persona"
            Dim comando As New MySqlCommand(query, connexio)
            Dim adaptador As New MySqlDataAdapter(comando)
            Dim conjunto_datos As New DataTable()
            adaptador.Fill(conjunto_datos)
            DataGridView1.DataSource = conjunto_datos
            DataGridView1.DataMember = "persona"
            connexio.Close()
        Catch
        End Try
    End Sub
    Private Sub desconnectar()
        connexio.Close()
    End Sub
    Private Sub Clicar1_Click(sender As Object, e As EventArgs) Handles Clicar1.Click
        Dim sanitat As Sanitari
        connectar1()
        afegirSanitat()
        eliminarSanitat()
        mostrarSanitat()
        desconnectar1()
    End Sub
    Private Sub afegirSanitat()
        Dim nom As String
        nom = Clicar.Text
        query = "INSERT INTO `sanitari` (`id`, `Nom Hospital`, `Num Habitació`, `Dia Entrada`, `Sortida`) VALUES ('12', 'Fundació Sant Hospital', '110', '20161007', '20170129')"
        Dim comanda = New MySqlCommand(query, connexio)
        comanda.ExecuteNonQuery()
    End Sub
    Private Sub eliminarSanitat()
        Dim eliminar As String
        eliminar = Clicar.Text
        eliminar = "DELETE FROM sanitari WHERE sortida='20190531'"
        Dim comanda = New MySqlCommand(eliminar, connexio)
        comanda.ExecuteNonQuery()
    End Sub
    Private Sub connectar1()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=sanitari"
            connexio.Open()
            MessageBox.Show("Connectat")
        Catch
            MessageBox.Show("Error")
        End Try
    End Sub
    Private Sub mostrarSanitat()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=sanitari"
            connexio.Open()
            query = "SELECT * FROM sanitari"
            Dim comando As New MySqlCommand(query, connexio)
            Dim adaptador As New MySqlDataAdapter(comando)
            Dim conjunto_datos As New DataTable()
            adaptador.Fill(conjunto_datos)
            DataGridView2.DataSource = conjunto_datos
            DataGridView2.DataMember = "sanitari"
            connexio.Close()
        Catch
        End Try
    End Sub
    Private Sub desconnectar1()
        connexio.Close()
    End Sub

    Private Sub Clicar2_Click(sender As Object, e As EventArgs) Handles Clicar2.Click
        Dim Aliment As Aliment
        connectar2()
        afegirAlimentació()
        mostrarAlimentació()
        eliminarAlimentació()
        desconnectar2()
    End Sub
    Private Sub afegirAlimentació()
        query = "INSERT INTO `aliment` (`id`, `Aliment`, `Quantitat Donada`, `Quantitat Rebuda`) VALUES ('12', '894', '1250', '9514')"
        Dim comanda = New MySqlCommand(query, connexio)
        comanda.ExecuteNonQuery()
    End Sub
    Private Sub eliminarAlimentació()
        Dim eliminar As String
        eliminar = Clicar.Text
        eliminar = "DELETE FROM aliment WHERE aliment='1250'"
        Dim comanda = New MySqlCommand(eliminar, connexio)
        comanda.ExecuteNonQuery()
    End Sub

    Private Sub connectar2()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=aliment"
            connexio.Open()
            MessageBox.Show("Connectat")
        Catch
            MessageBox.Show("Error")
        End Try
    End Sub
    Private Sub mostrarAlimentació()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=aliment"
            connexio.Open()
            query = "SELECT * FROM aliment"
            Dim comando As New MySqlCommand(query, connexio)
            Dim adaptador As New MySqlDataAdapter(comando)
            Dim conjunto_datos As New DataTable()
            adaptador.Fill(conjunto_datos)
            DataGridView2.DataSource = conjunto_datos
            DataGridView2.DataMember = "aliment"
        Catch
        End Try
    End Sub

    Private Sub desconnectar2()
        connexio.Close()
    End Sub
End Class