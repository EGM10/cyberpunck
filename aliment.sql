-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-11-2020 a las 20:48:10
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aliment`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aliment`
--

CREATE TABLE `aliment` (
  `id` int(100) NOT NULL,
  `Aliment` int(255) NOT NULL,
  `Quantitat Donada` int(255) NOT NULL,
  `Quantitat Rebuda` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `aliment`
--

INSERT INTO `aliment` (`id`, `Aliment`, `Quantitat Donada`, `Quantitat Rebuda`) VALUES
(1, 100, 50, 200),
(2, 50, 150, 100),
(4, 0, 0, 1268),
(5, 289, 70, 12),
(6, 358, 6, 589),
(7, 400, 600, 900),
(8, 87, 52, 0),
(9, 2684, 987, 8000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aliment`
--
ALTER TABLE `aliment`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
