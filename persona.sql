-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-11-2020 a las 20:48:46
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `persona`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `1rCognom` text NOT NULL,
  `2nCognom` text NOT NULL,
  `Població` text NOT NULL,
  `Data Naixament` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `Nom`, `1rCognom`, `2nCognom`, `Població`, `Data Naixament`) VALUES
(1, 'Edgar', 'Grau', 'Medeiros', 'Alinyà', 19980720),
(2, 'Federico', 'Garcia', 'Perez', 'Organyà', 18700130),
(3, 'Felix', 'Lopez', 'Mendoza', 'Coll de Nargè', 18750228),
(4, 'Carlos', 'Hernandez', 'Puig', 'Andorra', 18950305),
(5, 'Celia', 'Paños', 'Bueno', 'Oliana', 19901015),
(7, 'Roger', 'Tebas', 'Losada', 'Ponts', 20001210),
(8, 'Mariona', 'Rodriguez', 'Ruiz', 'Tremp', 20200707),
(9, 'Marina', 'Diaz', 'Muñoz', 'Osona', 20070817),
(10, 'Marta', 'Torres', 'Navarro', 'Osona', 20070817);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
