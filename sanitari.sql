-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-11-2020 a las 20:48:30
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sanitari`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sanitari`
--

CREATE TABLE `sanitari` (
  `id` int(11) NOT NULL,
  `Nom Hospital` text COLLATE utf8mb4_bin NOT NULL,
  `Num Habitació` int(11) NOT NULL,
  `Dia Entrada` int(11) NOT NULL,
  `Sortida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `sanitari`
--

INSERT INTO `sanitari` (`id`, `Nom Hospital`, `Num Habitació`, `Dia Entrada`, `Sortida`) VALUES
(1, 'Vall Ebron', 115, 20201126, 20210106),
(2, 'Arnau de Vilanova', 100, 20170726, 20180207),
(3, 'Hospital del Mar', 17, 20150412, 20180112),
(4, 'Hospital General de Catalunya', 1789, 20200403, 20200911),
(5, 'Hospital de Barcelona.', 310, 20200101, 20200202),
(7, 'Hospital Clínico y Provincial de Barcelona.', 300, 20191212, 20200216),
(8, 'Hospital Quirónsalud Barcelona', 750, 20170808, 20200903),
(9, 'Policlínica Barcelona', 1000, 20201225, 20210125),
(10, 'Clínica Tres Torres Barcelona', 1050, 20191027, 20213126);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sanitari`
--
ALTER TABLE `sanitari`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
