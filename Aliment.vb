﻿Imports MySql.Data.MySqlClient
Public Class Aliment
    Private Aliment As Int32
    Private quantitatRebuda As Int32
    Private quantitatDonada As Int32
    Public Sub New(ByVal Aliment As Int32, ByVal quantitatRebuda As Int32, ByVal quantitatDonada As Int32)
        Me.Aliment = Aliment
        Me.quantitatDonada = quantitatDonada
        Me.quantitatRebuda = quantitatRebuda
    End Sub
    Public Sub setAliment(ByVal Aliment As Int32)
        Me.Aliment = Aliment
    End Sub
    Public Function getAliment() As Int32
        Return Me.Aliment
    End Function
    Public Sub setQuantitatRebuda(ByVal quantitatRebuda As Int32)
        Me.quantitatRebuda = quantitatRebuda
    End Sub
    Public Function getQuantitatRebuda() As Int32
        Return Me.quantitatRebuda
    End Function
    Public Sub setQuantitatDonada(ByVal quantitatRebuda As Int32)
        Me.quantitatDonada = quantitatDonada
    End Sub
    Public Function getQuantitatDonada() As Int32
        Return Me.quantitatDonada
    End Function
End Class
