﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.CyberpunkCity = New System.Windows.Forms.PictureBox()
        Me.AnarFormulari2 = New System.Windows.Forms.Button()
        Me.logo = New System.Windows.Forms.PictureBox()
        CType(Me.CyberpunkCity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CyberpunkCity
        '
        Me.CyberpunkCity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CyberpunkCity.Image = CType(resources.GetObject("CyberpunkCity.Image"), System.Drawing.Image)
        Me.CyberpunkCity.Location = New System.Drawing.Point(0, 0)
        Me.CyberpunkCity.Name = "CyberpunkCity"
        Me.CyberpunkCity.Size = New System.Drawing.Size(802, 504)
        Me.CyberpunkCity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CyberpunkCity.TabIndex = 0
        Me.CyberpunkCity.TabStop = False
        '
        'AnarFormulari2
        '
        Me.AnarFormulari2.Location = New System.Drawing.Point(655, 454)
        Me.AnarFormulari2.Name = "AnarFormulari2"
        Me.AnarFormulari2.Size = New System.Drawing.Size(145, 50)
        Me.AnarFormulari2.TabIndex = 1
        Me.AnarFormulari2.Text = "Go"
        Me.AnarFormulari2.UseVisualStyleBackColor = True
        '
        'logo
        '
        Me.logo.Image = CType(resources.GetObject("logo.Image"), System.Drawing.Image)
        Me.logo.Location = New System.Drawing.Point(262, 12)
        Me.logo.Name = "logo"
        Me.logo.Size = New System.Drawing.Size(283, 85)
        Me.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.logo.TabIndex = 2
        Me.logo.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(802, 504)
        Me.Controls.Add(Me.logo)
        Me.Controls.Add(Me.AnarFormulari2)
        Me.Controls.Add(Me.CyberpunkCity)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.CyberpunkCity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents CyberpunkCity As PictureBox
    Friend WithEvents AnarFormulari2 As Button
    Friend WithEvents logo As PictureBox
End Class
