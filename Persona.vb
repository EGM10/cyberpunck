﻿Imports MySql.Data.MySqlClient
Public Class Persona
    Private nom As String
    Private primerCognom As String
    Private segonCognom As String
    Private poblacio As String
    Private dataNaixament As Int32

    Public Sub Persona(ByVal nom As String, v As String, v1 As String, ByVal nota As Int32, v2 As String)
        Me.nom = nom
        Me.primerCognom = primerCognom
        Me.segonCognom = segonCognom
        Me.poblacio = poblacio
        Me.dataNaixament = dataNaixament
    End Sub
    Public Sub setNom(ByVal nom As String)
        Me.nom = nom
    End Sub
    Public Function getNom() As String
        Return Me.nom
    End Function
    Public Function getPrimerCognom() As String
        Return Me.primerCognom
    End Function
    Public Sub setPrimerCognom(ByVal primerCognom As String)
        Me.primerCognom = primerCognom
    End Sub
    Public Function getSegonCognom() As String
        Return Me.segonCognom
    End Function
    Public Sub setSegonCognom(ByVal segonCognom As String)
        Me.segonCognom = segonCognom
    End Sub
    Public Function getPoblacio() As String
        Return Me.poblacio
    End Function
    Public Sub setPoblacio(ByVal poblacio As String)
        Me.poblacio = poblacio
    End Sub
    Public Function getDataNaixament() As Int32
        Return Me.dataNaixament
    End Function
    Public Sub setDataNaixament(ByVal dataNaixament As Int32)
        Me.dataNaixament = dataNaixament
    End Sub
End Class