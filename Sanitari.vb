﻿Imports MySql.Data.MySqlClient
Public Class Sanitari
    Private nom As String
    Private numeroHabitacio As Int32
    Private diaEntrada As Int32
    Private Sortida As Int32
    Public Sub New(ByVal nom As String, ByVal numeroHabitacio As Int32, ByVal diaEntrada As Int32, ByVal Sortida As Int32)
        Me.nom = nom
        Me.numeroHabitacio = numeroHabitacio
        Me.diaEntrada = diaEntrada
        Me.Sortida = Sortida
    End Sub
    Public Sub setNom(ByVal nom As String)
        Me.nom = nom
    End Sub
    Public Function getNom() As String
        Return Me.nom
    End Function
    Public Sub setNumeroHabitacio(ByVal numeroHabitacio As Int32)
        Me.numeroHabitacio = numeroHabitacio
    End Sub
    Public Function getnumeroHabitacio() As Int32
        Return Me.numeroHabitacio
    End Function
    Public Sub setDiaEntrada(ByVal diaEntrada As Int32)
        Me.diaEntrada = diaEntrada
    End Sub
    Public Function getDiaEntrada() As Int32
        Return Me.diaEntrada
    End Function
    Public Sub setSortida(ByVal sSortida As Int32)
        Me.Sortida = Sortida
    End Sub
    Public Function getSortida() As Int32
        Return Me.Sortida
    End Function
End Class

